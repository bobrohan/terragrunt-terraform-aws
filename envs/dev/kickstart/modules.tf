module "storage" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/storage?ref=master"
  bucket = "rohan-state"
}

module "dynamo_db" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/dynamo_db?ref=master"
  name = "rohan-state"
}