module "vpc" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/vpc?ref=master"
  cidr_block = "10.0.0.0/28"
}

module "internet_gateway" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/internet_gateway?ref=master"
  aws_vpc_id = module.vpc.aws_vpc_id
}

module "route_table" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/route_table?ref=master"
  internet_gateway_id = module.internet_gateway.internet_gateway_id
  aws_vpc_id = module.vpc.aws_vpc_id
  cidr_block = "0.0.0.0/0"
}

module "route_table_association" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/route_table_association?ref=master"
  aws_subnet_id = module.subnet.aws_subnet_id
  route_table_id = module.route_table.route_table_id
}

module "subnet" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/subnet?ref=master"
  availability_zone = "eu-west-2a"
  aws_vpc_id = module.vpc.aws_vpc_id
  cidr_block = "10.0.0.0/28"
}

module "rohan_key_pair" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/key_pair?ref=master"
  name = "rohan"
  pubkey = var.pubkey
}