resource "aws_security_group" "instance" {
  name        = "rohan-sec-group"
 
  vpc_id = module.vpc.aws_vpc_id
  
  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  
  ingress {
    cidr_blocks = [
      "10.0.0.0/28"
    ]
  
    from_port   = 0
    to_port     = 0
    protocol    = -1
  }
  
  # Terraform removes the default rule
  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
  }
}