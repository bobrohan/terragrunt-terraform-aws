output "aws_security_group_id" {
  value = aws_security_group.instance.id
}

output "aws_subnet_id" {
  value = module.subnet.aws_subnet_id
}

output "rohan_key_name" {
  value = module.rohan_key_pair.key_name
}