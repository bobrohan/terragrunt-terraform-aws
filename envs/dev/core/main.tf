terraform {
  required_version = "= 0.12.20"
  
  # Intentionally empty. Will be filled by Terragrunt.
  backend "s3" {} 
}

provider "aws" {
  version = "= 2.47.0"
  region      = "eu-west-2"
}