terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()

    arguments = [
      "-var-file=../terraform.tfvars"
    ]
  }
}

remote_state {
  backend = "s3"
  config = {
    bucket         = "rohan-state"
    key            = "envs/dev/${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-west-2"
    dynamodb_table = "rohan-state"
    encrypt        = true
  } 
}