terraform {
  required_version = "= 0.12.20"
  
  # Intentionally empty. Will be filled by Terragrunt.
  backend "s3" {} 
}

provider "aws" {
  version = "= 2.47.0"
  region      = "eu-west-2"
}

data "terraform_remote_state" "core" {
  backend = "s3"
  config = {
    bucket = "rohan-state"
    key    = "envs/dev/core/terraform.tfstate"
    region = "eu-west-2"
  }
}