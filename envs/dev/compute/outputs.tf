output "instance1_ip" {
  value = module.instance1_eip.instance_eip
}

output "instance2_ip" {
  value = module.instance2_eip.instance_eip
}