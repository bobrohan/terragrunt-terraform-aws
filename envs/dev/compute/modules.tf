module "instance1" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/compute?ref=master"
  name = "instance1"
  key_name = data.terraform_remote_state.core.outputs.rohan_key_name
  aws_security_group_id = data.terraform_remote_state.core.outputs.aws_security_group_id
  aws_subnet_id = data.terraform_remote_state.core.outputs.aws_subnet_id
  private_ip = "10.0.0.4"
  instance_type = "t2.medium"
}

module "instance1_eip" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/eip?ref=master"
  instance_id = module.instance1.instance_id
}

module "instance2" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/compute?ref=master"
  name = "instance2"
  key_name = data.terraform_remote_state.core.outputs.rohan_key_name
  aws_security_group_id = data.terraform_remote_state.core.outputs.aws_security_group_id
  aws_subnet_id = data.terraform_remote_state.core.outputs.aws_subnet_id
  private_ip = "10.0.0.5"
  instance_type = "t2.medium"
}

module "instance2_eip" {
  source = "git@gitlab.com:bobrohan/terraform-modules.git//aws/eip?ref=master"
  instance_id = module.instance2.instance_id
}