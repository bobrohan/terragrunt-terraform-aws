# Terragrunt

Simple terragrunt implementation, very nice

[Documentation](https://terragrunt.gruntwork.io/docs/getting-started/quick-start/)

## Usage

One time setup

```
cd envs/dev/kickstart
terraform init
terraform apply
```

Then on using 

```
cd envs/dev
terragrunt apply-all
``` 

or for example 

``` 
cd envs/dev/core
terragrunt apply
```